
/**This program displays the words Hello World in the console
 * @author Jaclyn Brassell
 *
 */
public class HelloWorld {

	/**this is the main method 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//prints "Hello World!" in the console
		System.out.print("Hello World!");
	}

}
